import json
import os
import app_root
import subprocess
import requests

config_path = os.path.join(app_root.path, 'config.json')

def fetch():
  config_file = open(config_path)
  return json.load(config_file)

def save(config):
  with open(config_path, 'w', encoding='utf-8') as f:
    json.dump(config, f, ensure_ascii=False, indent=4)

def update(data):
  settings = fetch()
  for key, value in data.items():
    settings[key] = value
  save(settings)

def reader_type():
  return get("reader_type")

def protocol():
  p = 'https' if get("secure") else 'http'
  return p

def server_url():
  return get("server_url")

def api_endpoint():
  return get('api_endpoint')

def api_key():
  return get('api_key')

def network_id():
  return get('network_id')

def getserial():
  # Extract serial from cpuinfo file
  cpuserial = "0000000000000000"
  try:
    f = open('/proc/cpuinfo','r')
    for line in f:
      if line[0:6]=='Serial':
        cpuserial = line[10:26]
    f.close()
  except:
    cpuserial = "ERROR000000000"
  return cpuserial

def get(key):
  settings = fetch()
  try: 
    return settings[key]
  except:
    return 'Unknown config.'
