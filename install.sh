#!/bin/bash

BASE_PATH=`pwd`
sed -i 's|working_directory|'$BASE_PATH'|' ./services/client_control.service
echo client control updated
sed -i 's|working_directory|'$BASE_PATH'|' ./services/rfid_trigger.service
echo rfid service updated
echo Installing updates and required packages.............................
echo ................................................................
sudo apt update
sudo apt upgrade

echo Enabling spi.........

sed -i 's/#dtparam=spi=on/dtparam=spi=on/g' /boot/config.txt
sed -i 's/#dtparam=i2c=on/dtparam=i2c=on/g' /boot/config.txt

echo Installing dependencies...

sudo apt install python3-dev python3-pip libnfc6 libnfc-bin libnfc-examples -y
sudo pip3 install spidev
sudo pip3 install mfrc522
sudo pip3 install -r requirements.txt

echo Adding services
sudo cp ./services/*.service /lib/systemd/system/

echo Enabling services
sudo systemctl enable client_control.service
sudo systemctl enable rfid_trigger.service

echo Reloading services 
sudo systemctl daemon-reload

echo ...................................................................
echo Installation completed please reboot to apply changes...
echo ..................................................................
