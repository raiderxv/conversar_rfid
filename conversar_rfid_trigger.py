#!/usr/bin/env python
import json
import requests
import urllib3
import setting as s
import RPi.GPIO as GPIO
from threading import Thread
from time import sleep
from mfrc522 import SimpleMFRC522
#from py532lib.i2c import *
#from py532lib.frame import *
from py532lib.constants import *
from py532lib.mifare import *
urllib3.disable_warnings()
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
BUZZER_PIN = 16
RED_PIN = 17
GREEN_PIN = 27
BLUE_PIN = 22
GPIO.setup(BUZZER_PIN, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(RED_PIN, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(GREEN_PIN, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(BLUE_PIN, GPIO.OUT, initial=GPIO.LOW)
config = s.fetch()
reader_type = config['reader_type']
server_url = config['server_url'] 
api_endpoint = config['api_endpoint']
ss_api_endpoint = config['ss_api_endpoint']
api_key = config['api_key']
network_id = config['network_id']
serial_no = s.getserial()
extra_params = json.loads(config['extra_params'])
last_id = None 
content_triggered = False
screensaver_triggered = False
reader = None
def call_api(id):
  print("API:" + id)
  data_obj = {'id': id, 'api_key': api_key, 'network_id': network_id, 'serial_no': serial_no}
  data_obj.update(extra_params)
  api_url = server_url + api_endpoint
  response = requests.post(api_url, data=data_obj, verify=False, timeout=5)
  data = response.json()
  print(data)
  if(data['success']):
    blue_on()
  else:
    yellow_on()

def trigger_screensaver():
  data_obj = {'api_key': api_key, 'network_id': network_id, 'serial_no': serial_no}
  data_obj.update(extra_params)
  ss_api_url = server_url + ss_api_endpoint
  print(ss_api_url)
  try:
    response = requests.post(ss_api_url, data=data_obj, verify=False, timeout=5)
    data = response.json()
    if(data is not None and data['success']):
      yellow_on()
    else:
      blue_on()
  except Exception as e:
    print(e)

def buzzer_on():
  GPIO.output(BUZZER_PIN, GPIO.HIGH)
  sleep(0.3)
  GPIO.output(BUZZER_PIN, GPIO.LOW)

def sync_call_api(id):
  p = Thread(target=call_api, args=(id, ))
  p.start()

def sync_buzzer_on():
  p = Thread(target=buzzer_on, args=())
  p.start()

def blue_on():
  GPIO.output(RED_PIN, GPIO.LOW)
  GPIO.output(GREEN_PIN, GPIO.LOW)
  GPIO.output(BLUE_PIN, GPIO.HIGH)

def yellow_on():
  GPIO.output(RED_PIN, GPIO.HIGH)
  GPIO.output(GREEN_PIN, GPIO.HIGH)
  GPIO.output(BLUE_PIN, GPIO.LOW)

def initialize_pn532_reader():
  reader = Mifare()
  reader.SAMconfigure()
  reader.set_max_retries(MIFARE_SAFE_RETRIES)
  return reader

def initialize_mfrc522_reader():
  reader = SimpleMFRC522()
  return reader

def initialize_reader():
  if reader_type.strip() == 'PN532':
    print("Initializing PN532...")
    return initialize_pn532_reader()
  else:
    print("Initializing MFRC522...")
    return initialize_mfrc522_reader()

def read_card():
  id = None
  if reader_type.strip() == "PN532":
    uid = reader.scan_field()
    if uid:
      id = str(int.from_bytes(uid, "big"))
      print(id)
  else:
    id, text = reader.read_no_block()
  return id

if __name__ == "__main__":
  try:
    reader = initialize_reader()
    blue_on()
    while True:
      id = read_card()
      if id is not None:
        if content_triggered == False:
          sync_call_api(id)
          sync_buzzer_on()
          yellow_on()
          content_triggered = True
          screensaver_triggered = False
          print("Content triggered")
        last_id = id
      else:
        if last_id is None and screensaver_triggered == False:
          screensaver_triggered = True
          content_triggered = False
          trigger_screensaver()
          print("Screensaver mode")
        last_id = None
  except KeyboardInterrupt:
    print("Keyboard interrupt")
  except:
    print("Program error.")
  finally:
    GPIO.cleanup()
