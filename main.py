import json
import os
import subprocess
import requests
import setting as s
import ctypes
# from Queue import Queue
from flask import Flask, session, redirect, url_for, escape, request, jsonify, render_template
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

app.secret_key = 'idontcaremuchonthiskey'
app.config['SESSION_TYPE'] = 'memcached'
 


@app.route("/")
def index():
    if 'ctusername' in session:
        config = s.fetch()
        return render_template('settings.html', config=config, serial_no=s.getserial())
    else:
        return redirect(url_for('login'))

# Save settinguration setting
@app.route("/settings", methods=['POST', 'GET'])
def setting():
    if request.method == 'POST' and 'ctusername' in session:
        params = request.form
        server_url = params.get("server_url")
        reader_type = params.get("reader_type")
        api_endpoint = params.get('api_endpoint')
        api_key = params.get('api_key')
        ss_api_endpoint = params.get('ss_api_endpoint')
        network_id = params.get('network_id')
        extra_params = params.get('extra_params')
        if not extra_params:
            extra_params = "{}"

        s.update({"server_url": server_url, "reader_type": reader_type, "api_endpoint": api_endpoint, "ss_api_endpoint": ss_api_endpoint, "api_key": api_key, "network_id": network_id, "extra_params": extra_params})
        return render_template('settings.html', message="Settings saved.", config=s.fetch(), serial_no=s.getserial())
    else:
        return redirect(url_for('index'))

@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        config = s.fetch()
        username = request.form.get('username')
        password = request.form.get('password')
        if username == config["username"] and password == config["password"]:
            session['ctusername'] = username
            return redirect(url_for('index'))
        else:
            return render_template('login.html', error='Username or password did not match.')
    else:
        return render_template('login.html')

@app.route("/logout")
def logout():
    session.pop('ctusername', None)
    return redirect(url_for('login'))



if __name__ == "__main__":
    try:
        app.run(host="0.0.0.0", port=8080, debug=False)
    except KeyboardInterrupt:
        print("Exit")
